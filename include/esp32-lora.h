#ifndef _LORA32_H__
#define _LORA32_H__

#include "driver/spi_common.h"
#include "driver/spi_master.h"
#include "freertos/event_groups.h"

#define REG_FIFO                 0x00
#define REG_OP_MODE              0x01
#define REG_BR_MSB               0x02
#define REG_BR_LSB               0x03
#define REG_FD_MSB               0x04
#define REG_FD_LSB               0x05
#define REG_FRF_MSB              0x06
#define REG_FRF_MID              0x07
#define REG_FRF_LSB              0x08
#define REG_PA_CONFIG            0x09
#define REG_LNA                  0x0c
#define REG_FIFO_ADDR_PTR        0x0d
#define REG_FIFO_TX_BASE_ADDR    0x0e
#define REG_FIFO_RX_BASE_ADDR    0x0f
#define REG_FIFO_RX_CURRENT_ADDR 0x10
#define REG_IRQ_FLAGS            0x12
#define REG_RX_NB_BYTES          0x13
#define REG_PKT_RSSI_VALUE       0x1a
#define REG_PKT_SNR_VALUE        0x1b
#define REG_MODEM_CONFIG_1       0x1d
#define REG_MODEM_CONFIG_2       0x1e
#define REG_PREAMBLE_MSB         0x20
#define REG_PREAMBLE_LSB         0x21
#define REG_PAYLOAD_LENGTH       0x22
#define REG_MODEM_CONFIG_3       0x26
#define REG_RSSI_WIDEBAND        0x2c
#define REG_DETECTION_OPTIMIZE   0x31
#define REG_DETECTION_THRESHOLD  0x37
#define REG_SYNC_WORD            0x39
#define REG_DIO_MAPPING_1        0x40
#define REG_VERSION              0x42
#define REG_PA_DAC               0x4D

// modes
#define MODE_SLEEP               0x00
#define MODE_STANDBY             0x01
#define MODE_TX                  0x03
#define MODE_RX_CONTINUOUS       0x05
#define MODE_RX_SINGLE           0x06
#define MODE_CAD_DETECT          0x07
#define MODE_LONG_RANGE_MODE     0x80

// PA config
#define PA_BOOST                 0x80

// IRQ masks
#define IRQ_RX_TIMEOUT             0x80
#define IRQ_RX_DONE                0x40
#define IRQ_PAYLOAD_CRC_ERROR      0x20
#define IRQ_VALID_HEADER           0x10
#define IRQ_TX_DONE                0x08
#define IRQ_CAD_DONE               0x04
#define IRQ_FHSS_CHANGE            0x02
#define IRQ_CAD_DETECTED           0x01

#define MAX_PKT_LENGTH             255

#define DETECT_OPT_SF6             0xC5
#define DETECT_THRES_SF6           0x0C
#define DETECT_OPT_OTHER           0xC3
#define DETECT_THRES_OTHER         0x0A

#define DEFAULT_SF 7
#define DEFAULT_PREAMBLE 8
#define DEFAULT_CR 5

#define DIO0_MODE_RXDONE  0x00
#define DIO0_MODE_TXDONE  0x40
#define DIO0_MODE_CADDET  0x80

#define EV_DIO0 (1 << 0)

enum freq {
  F433, F866, F915
} lora32_freq;

typedef enum {
  B78, B104, B156, B208, B3125, B417, B625, B125, B250, B500
} bandwidth;

const long long frequencies[3];
const long bandwidths[10];

typedef void (*receiveCallback)(uint8_t size);
typedef void (*txdoneCallback)();
typedef void (*cadDoneCallback)(bool detected);
typedef void (*cadDetectedCallback)();

typedef struct {
  EventGroupHandle_t events;
} lora32_handle_t;

typedef struct lora32_cfg_t {
  uint8_t nss;
  uint8_t dio0;
  uint8_t reset;
  uint8_t fifoIdx;

  long frequency;
  uint8_t bandwidth;

  uint8_t spreadingFactor;
  uint8_t codingRate;
  uint16_t preamble;

  bool useCRC;
  bool implicitHeader;

  receiveCallback receive;
  txdoneCallback tx_done;
  cadDoneCallback cad_done;
  cadDetectedCallback cad_detected;

  spi_device_handle_t spi;

  lora32_handle_t handle;
} lora32_cfg_t;

lora32_cfg_t lora32_create();
uint8_t lora32_init(lora32_cfg_t *config);
uint8_t lora32_data_available(lora32_cfg_t *lora);

double lora32_calc_datarate(lora32_cfg_t *lora);
void lora32_dump_regs(lora32_cfg_t *lora);
void lora32_enable_continuous_rx(lora32_cfg_t *lora);
void lora32_enable_cad(lora32_cfg_t *lora);
void lora32_send(lora32_cfg_t *config, uint8_t *data, uint8_t len);
void lora32_set_bandwidth(lora32_cfg_t *lora, uint8_t bw);
void lora32_set_coding_rate(lora32_cfg_t *lora, uint8_t cr);
void lora32_set_spreadfactor(lora32_cfg_t *lora, uint8_t factor);
void lora32_read_data(lora32_cfg_t *lora, uint8_t *data);

#endif // _LORA32_H__
