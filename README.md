# ESP32 LoRa (SX127*)

Provides SPI driver for SX1276/SX1278 LoRa radio

## Install

Designed to be used as an `esp-idf component`. Suggested usage is with  `git submodule`

```
git submodule add https://gitlab.com/morganrallen/esp32-lora.git components/esp32-lora/
git submodule init
git submodule update
```

## Configure

Uses built in KConfig for configuration.
Run `make menuconfig` and find config under `Component config` -> `LORA32`.
Defaults targeted to TTGO LoRa OLED boards.

## Use
This is the most basic usage. Setting up the LoRa instance, setting it's receive handler, running init then sending a message.

```
  #include <stdio.h>
  #include <string.h>

  #include "esp32-lora.h";

  lora32_cfg_t lora;

  static void handle_lora_receive(uint8_t size) {
    char *message = malloc(size+1);

    lora32_read_data(&lora, message);

    // ensure null termination
    message[size] = '\0';
    
    printf("msg: %s\n", message);
  }

  void app_main() {
    lora = lora32_create();
    lora.receive = &handle_lora_receive;

    lora32_init(&lora);

    lora32_send(&lora, "Hello, LoRa", 11);
  }

```

## API

### `static lora32_cfg_t lora32_create()`
Creates a new LoRa instance.

### `uint8_t lora32_init(lora32_cfg_t *config)`
Initialized LoRa instance. This configures GPIOs, SPI, LoRa radio and receive handlers.

Returns 1 on success.

### `void lora32_set_bandwidth(lora32_cfg_t *lora, enum bandwidth bw)`
Set operating bandwidth. Accepts enum bandwidth
`B78`, `B104`, `B156`, `B208`, `B3125`, `B417`, `B625`, `B125`, `B250`, `B500` or `0`-`9`

### `void lora32_set_coding_rate(lora32_cfg_t *lora, uint8_t d)`
Set `Coding Rate`.

Accepts `5`-`8`.

### `uint8_t lora32_data_available(lora32_cfg_t *lora)`
Returns number of bytes available in FIFO buffer.

### `void lora32_send(lora32_cfg_t *config, uint8_t *data, uint8_t len)`
Transmits data over the LoRa radio. Puts `DIO0` into `TXDONE` mode.

### `void lora32_set_spreadfactor(lora32_cfg_t *lora, uint8_t factor)`
Sets LoRa Spread Factor.

Accepts `6`-`12`.

### `double lora32_calc_datarate(lora32_cfg_t *lora);`
Returns data rate in Bits Per Second (bps) for given `lora32_cfg_t` configuration.

### `void lora32_dump_regs(lora32_cfg_t *lora)`
Dumps all registers from SX1276.

### `void lora32_read_data(lora32_cfg_t *lora, uint8_t *data)`
Reads data out of FIFO buffer. This would typically be called after the callback `received` is triggered.

### `void lora32_enable_continuous_rx(lora32_cfg_t *lora)`
Enables continuous receive mode. If a receive callback has been setup, it will be trigger
on any incoming data until either an `OP_MODE` or `DIO0` mode are change.

### `void lora32_enable_single_rx(lora32_cfg_t *lora)`
* **NOTE** This requires `DIO1` for complete functionality.
* **NOTE** Implenentation incomplete.

Enables single receive mode. Will either trigger `RXDONE` on `DIO0` or `RXTIMEOUT` on `DIO1`

### `void lora32_enable_cad(lora32_cfg_t *lora)`
Enables Channel Activity Detection. Will trigger callback `cad_done` with `bool detected` on completion.
If activity was detected, it will also trigger `cad_detected`


## Callbacks
`lora32_cfg_t` provides several callbacks based on `DIO0` interrupts. Most functionality is provided through
these, favoring async operations over syncronous polling.

### `lora32_cfg_t->receive(uint8_t size)`
Triggered from `RXCONTINUOUS` and `RXSINGLE` modes. Provides the number of bytes available. Should be followed
up with a call to `lora32_read_data` to retrieve data from FIFO buffer.

### `lora32_cfg_t->tx_done()`
Triggers after call to `lora32_send` completes, as long as `DIO0` mode has not been changed.

### `lora32_cfg_t->cad_done(bool detected)`
Triggers after call to `lora32_enable_cad` completes (ie times out) or channel activity is detected.
`bool detected` will reflect these two states.

### `lora32_cfg_t->cad_detected()`
Triggers after call to `lora32_enable_cad` successfully detects channel activity.
